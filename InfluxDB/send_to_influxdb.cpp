/* --- Standard Includes --- */
#include <iostream>
#include <fstream>
#include <string>

/* --- Includes for using rapidJSON library to parse JSON --- */
#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <rapidjson/ostreamwrapper.h>

/* --- Includes ShunyaOS Interface---*/
#include <shunyaInterfaces.h>

using namespace rapidjson;
using namespace std;

string parseJSON(string key1, string key2)//Function returns the value from the JSON file according to the keys provided
{	
	ifstream conf("./../Configuration JSON/config.json");//open the json file
	IStreamWrapper json(conf);//store the json file
	Document d;
	d.ParseStream(json); // parse the json file
	return(d[key1.c_str()][key2.c_str()].GetString());//return the value string for the key
}


void send_data(int voltage, char *unix_timestamp)
{
	influxdbObj influxdb = newInfluxdb(parseJSON("influxdb","db name"));
	writeDbInflux (influxdb, "measurement, value=%d %ld",voltage, unix_timestamp);
}

int main(void)
{
	initLib();
	send_data(voltage, unix_timestamp);
 /* the data (i.e. voltage and UNIX timestamp) would be got from the JSON file (i.e. after conversion of raw data into JSON format). */
	return(0);
}
