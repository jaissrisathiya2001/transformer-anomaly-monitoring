## **InfluxDB** :  InfluxDB is an open-source time series database developed by InfluxData. It is written in Go and optimized for fast, high-availability storage and retrieval of time series data in fields such as operations monitoring, application metrics, Internet of Things sensor data, and real-time analytics.   
### InfluxDB OSS  
* Download InfluxDB OSS
* Get started with InfluxDB OSS
* Install InfluxDB OSS  

![influx](https://www.influxdata.com/wp-content/uploads/influxdb-dashboard.png)  

### Brief Glossary about InfluxDB:  
* **Aggregation**: An InfluxQL function that returns an aggregated value across a set of points
* **Batch**: A collection of data points in InfluxDB line protocol format, separated by newlines 
* **Continuous query (CQ)**: An InfluxQL query that runs automatically and periodically within a database.
* **Database**: A logical container for users, retention policies, continuous queries, and time series data.
* **Duration**: The attribute of the retention policy that determines how long InfluxDB stores data. Data older than the duration are automatically dropped from the database. 
* **InfluxDB line protocol**: The text based format for writing points to InfluxDB.
* **Node** :An independent influxd process.
* **Point**: In InfluxDB, a point represents a single data record, similar to a row in a SQL database table. Each point has a measurement, a tag set, a field key, a field value, and a timestamp; is uniquely identified by its series and timestamp.
* **Schema**: How the data are organized in InfluxDB. The fundamentals of the InfluxDB schema are databases, retention policies, series, measurements, tag keys, tag values, and field keys. 
* **Series key**: A series key identifies a particular series by measurement, tag set, and field key.
* **Server**: A machine, virtual or physical, that is running InfluxDB. There should only be one InfluxDB process per server.
* **Shard**: A shard contains the actual encoded and compressed data, and is represented by a TSM file on disk. Every shard belongs to one and only one shard group. Multiple shards may exist in a single shard group. Each shard contains a specific set of series. All points falling on a given series in a given shard group will be stored in the same shard (TSM file) on disk.
* **Timestamp**: The date and time associated with a point. All time in InfluxDB is UTC.
* **TSM (Time Structured Merge tree)**: The purpose-built data storage format for InfluxDB. TSM allows for greater compaction and higher write and read throughput than existing B+ or LSM tree implementations.
* **User**: There are two kinds of users in InfluxDB: Admin users have READ and WRITE access to all databases and full access to administrative queries and user management commands, Non-admin users have READ, WRITE, or ALL (both READ and WRITE) access per database.
 

### Tools

*  **Influx command line interface (CLI)**
The InfluxDB command line interface (influx) includes commands to manage many aspects of InfluxDB, including databases, organizations, users, and tasks.

* **Influxd command**
The influxd command starts and runs all the processes necessary for InfluxDB to function.

* **Influx Inspect disk shard utility**
Influx Inspect is a tool designed to view detailed information about on disk shards, as well as export data from a shard to line protocol that can be inserted back into the database.

* **InfluxDB inch tool**
Use the InfluxDB inch tool to test InfluxDB performance. Adjust metrics such as the batch size, tag values, and concurrent write streams to test how ingesting different tag cardinalities and metrics affects performance.

* **Graphs and dashboards**
Use Chronograf or Grafana dashboards to visualize your time series data.  

![lineprotocol](https://kapibara-sos.net/wp-content/uploads/2018/09/inflx01-1024x494.png)

## Telegraf plugins
Telegraf is a plugin-driven agent that collects, processes, aggregates, and writes metrics. It supports four categories of plugins including input, output, aggregator, and processor.

###Tools:  
* **Influx CLI**  
Use the influx CLI to interact with and manage both InfluxDB Cloud and InfluxDB OSS. Write and query data, generate InfluxDB templates, export data, manage organizations and users, and more.

* **Influxd CLI**  
Use the influxd CLI to start the InfluxDB OSS server and manage the InfluxDB storage engine. Restore data, rebuild the time series index (TSI), assess the health of the underlying storage engine, and more.
