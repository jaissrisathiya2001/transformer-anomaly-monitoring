##MQTT Component

#MQTT (Message Queuing Telemetry Transport):
A simple messaging protocol designed for constrained devices with low bandwidth.
A lightweight publish and subscribe system to publish and receive messages as a client.
A perfect solution for IOT applications, also allows to send commands to control outputs, read and publish data from sensor nodes.

#A general overview of how communication takes place within MQTT devices
MQTT Client as a publisher sends a message to the MQTT broker whose work is to distribute the message accordingly to all other MQTT clients subscribed to the topic on which publisher publishes the message.

#MQTT configuration in Shunya#
Config      	   Description
MQTT broker url	  MQTT broker url
Username	      MQTT broker username for security (optional)
Password	      MQTT broker password for security (optional)
Client ID	      Unique Identification of the client, user can put any name that is convenient.

##MQTT API#
#newMqtt()#

Description : Creates a new MQTT Instance
Parameters
name (char *) - Name of the JSON object.
Return-type : mqttObj
Returns : mqttObj Instance for MQTT
Usage :

For example: Lets say your JSON file looks like
"mqttbroker1": {
    "brokerUrl": "192.168.0.1:8080",
    "clientID": "shunya pi3",
}
So the usage of the API's will be
mqttObj broker1 = newMqtt("mqttbroker1");

#mqttConnect()#

Description: Connects to given MQTT broker with details given in the settings

Parameters
*obj(mqttObj) - Pointer to the MQTT Instance.
Return-type : void
Usage :
/* Create New Instance */
mqttObj broker1 = newMqtt("mqttbroker1");
/* Connect to broker */
mqttConnect(&broker1) ;

#mqttPublish()#
Description: Publishes the data to the MQTT broker
Parameters
*obj(mqttObj) - Pointer to the MQTT Instance.
*topic(char) - Topic the Message/data needs to be sent.
*fmt(const char) - Message/data that needs to be sent.
Return-type : void
Usage :
/* Create New Instance */
mqttObj broker1 = newMqtt("mqttbroker1");
/* Connect to broker */
mqttConnect(&broker1) ;
/* Publish to topic */
mqttPublish(&broker1, "topic","message %d", number) ;

#mqttSubscribe()#
Description: Subscribes to the given topic and callback can be used to run a user-defined function when the devices receive a message from the topic Parameters
*obj(mqttObj) - Pointer to the MQTT Instance.
*topic(char) - Topic the Message/data needs to be sent.
void (*callback)(int, char *, int, char *) - The pointer to the callback function
Return-type : void

Usage :

/* Create New Instance */
mqttObj broker1 = newMqtt("mqttbroker1");
/* Connect to broker */
mqttConnect(&broker1) ;
/* Subscribe to topic */
mqttSubscribe(&broker1, "test1") ;

#mqttSetSubCallback()#

Description: Set the subscribe callback function that executes the when the topic subscribed gets a message.

The Callback function arguments need to be in the specified format,

void callback(int topicLen, char *topic, int msgLen, char *message)
This format is used to pass the message received to the callback function.

Parameters

*obj(mqttObj) - Pointer to the MQTT Instance.
callback (void *) - A callback function that will get called when the subscribed topic gets a message
Return-type : void

Usage :

/* Create New Instance */
mqttObj broker1 = newMqtt("mqttbroker1");

void callback(int topicLen, char *topic, int msgLen, char *message)
{
    printf("Received Message is: %s", message);
}

mqttSetSubCallback(&broker1, callback);

#mqttDisconnect()#
Description: Disconnects the MQTT Broker

Parameters:

*obj(mqttObj) - Pointer to the MQTT Instance.
Return-type : void

Usage :
mqttDisconnect(&broker1) ;
